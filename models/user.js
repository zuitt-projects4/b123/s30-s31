/*
	Mongoose Schema

		Schema
			- in mongoose context, determine the structure of the documents.
			- It acts as a blueprint to how data/entries in our collection should look like.
			-Gone str the days when worry if entry has "stock" or "stocks" as fieldname.
			-Mongoose itself will notify us and disallow us to create documents that do not match the schema.
			- a constructor from mongoose that will allow us to create a new Schema object.

			.Schema() is a constructor from  mongoose that will allow us to create a new Schema object.
				-define the fields for the task documents.
				-this tasks document has a name and status field.
				- name with the value to be String.
				- status is {} and has fields to follow:
					-define what will happen to the type.
					- value's type: string 
					and if not provided a value once the document was created it will 
					- default to value: "pending"
*/

const mongoose = require("mongoose")


const userSchema = new mongoose.Schema(
	{
		username: String,
		password: String
	}
)

module.exports = mongoose.model("User", userSchema)
