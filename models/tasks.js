const mongoose = require("mongoose")
/*
	Task is a Model

	MOngoose Model
		- is used to connect your api to the corresponding collection in your database. 
		- It is a representation of the Task documents to be created into a new task collection.
		- uses schemas to create/instantiates objects that corresponds to the schema.
		- by default, when creating the collection from your model, the collection name will be pluralized(ENGLISH ONLY)

		mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)

		-your models have methods that will help in querying, creating or even editing your documents.
		- its also are constructors of your object. it will then create an object with their own methods.

		let <varName> = new <modelName>({<properties>: <values>}) - created from Task Model
			- has added method for use in our application,
			-.save(<error>,<save>) - is a method from an object created by a model.
				- allow us to save our document into our collection.
				- can have an anonymous function and this can have 2 parameters
				- the first item <saveErr> receive an error object if there was an error creating our document
				- the second item <saveTask> saved documents.
		

*/
const taskSchema = new mongoose.Schema(
	{
		name: String,
		status:{
			type: String,
			default: "pending"
		}
	}
)

module.exports =  mongoose.model("Task", taskSchema)
/*
	module.exports will allow us to export file/functions/values into another js file within our appliacation.
	- export the model into other files.
*/