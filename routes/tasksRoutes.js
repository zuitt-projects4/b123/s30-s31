
const express = require("express")
const router = express.Router()
/*
	Router() from express, allows us to access to HTTP method routes.
		- it will act as a middleware and our routing  system
		- should only be concerned with our endpoints and our methods.
		- the action should be done once a route is accessed should be in separate file, it should be in our controller.
*/
const tasksController = require('../controllers/taskControllers')
const {createTasksController, getAllTasksController, getSingleTasksController, updateStatusCompleteController, updateStatusCancelController} = tasksController

router.post('/', createTasksController)
router.get('/', getAllTasksController)
router.get('/:id', getSingleTasksController)
router.put('/complete/:id', updateStatusCompleteController)
router.put('/cancel/:id', updateStatusCancelController)


//Router holds all of our routes and can be exported and imported into another file.
module.exports = router