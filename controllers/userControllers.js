
const User = require('../models/user')

module.exports.createUserController = (req, res) =>{

	User.findOne({username: req.body.username}) 
	.then(result =>{
		
		if(result !== null && result.username === req.body.username){
			
			return res.send("Duplicate Username Found")

		}else{

			let newUser = new User(
				{
					username: req.body.username, 
					password: req.body.password
				}
			)

			newUser.save() 
			.then(saveUser=>res.send(result))
			.catch(err => res.send(err))
		}
	})
	.catch(err => res.send(err))
}

module.exports.getAllUsersController = (req, res) => {
	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


module.exports.getSingleUserController = (req , res) => {
	/*
		mongoose has a query called .findById() which works like .find({_id: "id"})
		req.params.id = id to be passed from your url.
	*/
	console.log(req.params.id)
	User.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.updateSingleUserController =(req,res)=>{
	/*
		models.findByIdAndUpdate(<id>, <updates>, <new:true>) - will do 2 things, first look for the single item by its id, then add the update.
		-updates varible always as an objects
		- will contain the field and the value we want to update
		By default, without the thied argument, findByIdAndUpdate() returns the document before the updates were updated.
		- {new:true} it returns the updated document.

	*/
	console.log(req.params.id)

	let updates = {username: req.body.username}

	User.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then( updatedUser => res.send(updatedUser))
	.catch(err => res.send(err))

}