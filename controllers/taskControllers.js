/*
	import tasks models.
*/
const Task = require('../models/tasks')

module.exports.createTasksController = (req , res) =>{

	console.log(req.body)
	/*
		checking if there is a documents with duplicate name field
		Model.findOne() is a mongoose method similar to findOne() in MongoDB.
			- we can process the result via our api
			Model.findOne(<criteria>, <anonymous function(error, result)>)
			err - will return null if there is no error
			result - will return null if there is no documents were found with the name given in the criteria.
			if() - send a message to the client if there is a duplicate in the document 
	*/
	/*
		.then() - is able to capture the result of our query
				- is able to process the result and when that result is returned, we can actually add another .then() to process the next result.
		.catch() - is able to capture the error of our query

	*/
	Task.findOne({name: req.body.name})
	.then(result =>{

		console.log(result)
		if(result !== null && result.name === req.body.name){
			
			return res.send("Duplicate Task Found")

		}else{

			let newTask = new Task({name: req.body.name})

			newTask.save()
			.then(result => res.send(result))
			.catch(err => res.send(err))
		}

	})
	.catch(err => res.send(err))
}

module.exports.getAllTasksController = (req , res) => {
	/*
		-model.find() is a mongoose method similar to mongoDB's find()
		- it is able to retrieve all documents that will match the criteria
	*/	
	Task.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.getSingleTasksController =(req, res) =>{

	console.log(req.params.id)
	Task.findById(req.params.id, {_id:0, name:1})
	.then(result => res.send(result))
	.catch(err => res.send(err))

}

module.exports.updateStatusCompleteController =(req, res) =>{

	console.log(req.params.id)
		
	let updates = {status: "complete"}

	Task.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updateStatus => res.send(updateStatus))
	.catch(err => res.send(err))
}

module.exports.updateStatusCancelController = (req, res) =>{
	console.log(req.params.id)
		
	let updates = {status: "cancelled"}

	Task.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updateStatus => res.send(updateStatus))
	.catch(err => res.send(err))

}