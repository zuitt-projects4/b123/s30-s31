const express = require("express")
const mongoose = require("mongoose")
const app = express()

const port = 4000
/*
	mongodb connection string to the connect(<copied urlin mongoDB atlas>, 
	<object>
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
	) method
	change <password> to your database password
	change MyFirstDatabase to todoList123
	
*/
mongoose.connect("mongodb+srv://louiseb123:gKg58tTCuFuIXHR@cluster0.o89xz.mongodb.net/todoList123?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
)

/*
	notifications if the connection to the database is success or fail.
	console.error.bind(console, <message>) - print error in both terminal and browser
	db.once("open", <function>) - output message in the terminal if the connected is successful.

	app.use(express.json()) - middleware, in expressjs context, are methods, functions that acts and adds features to your applications,
		- handle json data from our clients.
*/
let db = mongoose.connection
db.on("error", console.error.bind(console, "connection error."))
db.once("open", () => console.log("Connected to MongoDB"))

app.use(express.json())

const tasksRoutes = require('./routes/tasksRoutes')
console.log(tasksRoutes)
app.use('/tasks', tasksRoutes)
//a middleware to group all of our routes starting their endpoints with /tasks

const userRoutes = require('./routes/userRoutes')
console.log(userRoutes)
app.use('/users', userRoutes)


app.get('/hello', (req , res) => {

	res.send("Hello from our new Express API.")
})

app.listen(port, () => console.log(`Server is running at port ${port}`))
